//
//  mainmenu.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 19/05/24.
//

import SwiftUI

struct minigame : View {
    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                HStack{
                    NavigationLink(destination: quest().navigationBarBackButtonHidden(true)) {
                        Text("Coloring Game")
                            .padding()
                            .background(Color.white)
                            .foregroundColor(.black)
                            .cornerRadius(10)
                    }
                    
                    NavigationLink(destination: Contentvieww().navigationBarBackButtonHidden(true)) {
                        Text("Guess?")
                            .padding()
                            .background(Color.white)
                            .foregroundColor(.black)
                            .cornerRadius(10)
                    }
                }
            }
            .background(
                Image("color")
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
            )
        }
        .preferredColorScheme(.light)
    }
}



#Preview {
    minigame()
}
