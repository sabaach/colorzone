//
//  gifimage.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 25/05/24.
//

import SwiftUI
import UIKit
import FLAnimatedImage

struct gifimage: UIViewRepresentable {
    let gifName: String

    func makeUIView(context: Context) -> FLAnimatedImageView {
        let gifImageView = FLAnimatedImageView()
        gifImageView.contentMode = .scaleAspectFill
        gifImageView.clipsToBounds = true

        if let path = Bundle.main.path(forResource: gifName, ofType: "gif"),
           let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
           let gifImage = FLAnimatedImage(animatedGIFData: data) {
            gifImageView.animatedImage = gifImage
        }

        return gifImageView
    }

    func updateUIView(_ uiView: FLAnimatedImageView, context: Context) {
        // No update needed
    }
}

