//
//  quest.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 18/05/24.
//

import SwiftUI

struct quest : View {
    var width = 300.5
    var height = 300.5
    
    var width1 = 100.5
    var height2 = 50.5
    
    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                Text("REMEMBER THIS IMAGE!!!")
                    .font(Font.system(size: 30))
                    .padding()
                    .foregroundColor(.red)
                    .cornerRadius(10)
                    .bold()
                Spacer()
                HStack {
                    Image("coloredbg")
                        .resizable()
                        .scaledToFit()
                        .frame(width: width, height: height)
                        .padding()
                }
                Spacer()
                NavigationLink(destination: ContentView().navigationBarBackButtonHidden(true)) {
                    Text("START")
                        .padding()
                        .frame(width: width1, height: height2)
                        .background(Color.cyan)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
                Spacer()
            }
        }
        .preferredColorScheme(.light)
        .onAppear {
                    AudioManager.shared.stopBackgroundMusic()
        }
    }
}

#Preview {
    quest()
}
