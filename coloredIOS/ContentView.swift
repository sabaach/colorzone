//
//  ContentView.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 17/05/24.
//

import SwiftUI
import UIKit
import CoreML
import Vision

struct ContentView: View {
    @State private var drawingColor: Color = .clear
    @State private var isDrawing = false
    @State private var circles: [CircleData] = []
    var width = 450.5
    var height = 450.5
    @State private var predictionResult = ""
    @State private var predictionAccuracy = 0
    @State private var showingResultView = false
    @State private var screenshotImage: UIImage?
    @State private var circleSize: CGFloat = 20
    
    var body: some View {
        NavigationStack {
            ZStack {
                VStack {
                    Spacer()
                    HStack {
                        Image("polosbg")
                            .resizable()
                            .scaledToFit()
                            .frame(width: width,height: height)
                            .gesture(DragGesture(minimumDistance: 0)
                                .onChanged { value in
                                    isDrawing = true
                                    let touchPoint = value.location
                                    circles.append(CircleData(center: touchPoint, color: drawingColor, size: circleSize))
                                }
                                .onEnded { value in
                                    isDrawing = false
                                })
                    }
                    Spacer()
                    
                    HStack {
                        Button(action: {
                            drawingColor = .gray
                        }) {
                            Text(" ")
                                .frame(width: 50, height: 50)
                                .background(Color.gray)
                                .cornerRadius(10)
                        }
                        
                        Button(action: {
                            drawingColor = .brown
                        }) {
                            Text(" ")
                                .frame(width: 50, height: 50)
                                .background(Color.brown)
                                .cornerRadius(10)
                        }
                        
                        Button(action: {
                            drawingColor = .black
                        }) {
                            Text(" ")
                                .frame(width: 50, height: 50)
                                .background(Color.black)
                                .cornerRadius(10)
                        }
                        
                        Button(action: {
                            drawingColor = .blue
                        }) {
                            Text(" ")
                                .frame(width: 50, height: 50)
                                .background(Color.blue)
                                .cornerRadius(10)
                        }
                        
                        Button(action: {
                            drawingColor = .white
                        }) {
                            Image(systemName: "circle.fill")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 50, height: 50)
                                .foregroundColor(.white)
                                .background(Color.gray)
                                .cornerRadius(10)
                        }
                    }
                    
                    Spacer()
                    NavigationLink(destination: ResultView(accuracy: predictionAccuracy, predictionResult: predictionResult, image: screenshotImage).navigationBarBackButtonHidden(), isActive: $showingResultView) {
                        EmptyView()
                    }
                    
                    Button(action: {
                        print("Matched")
                        guard let screenshot = takeScreenshot() else { return }
                        self.screenshotImage = screenshot
                        processImageWithML(image: screenshot)
                    }) {
                        Text("Match Image")
                            .frame(width: 280, height: 50)
                            .foregroundColor(.white)
                            .background(Color.cyan)
                            .cornerRadius(10)
                    }
                    Spacer()
                }
                .padding()
                .background(
                    GeometryReader { geometry in
                        ForEach(circles, id: \.id) { circleData in
                            Circle()
                                .fill(circleData.color)
                                .frame(width: circleData.size, height: circleData.size)
                                .position(circleData.center)
                        }
                        .gesture(
                            DragGesture(minimumDistance: 0)
                                .onChanged { value in
                                    if isDrawing {
                                        let touchPoint = value.location
                                        circles.append(CircleData(center: touchPoint, color: drawingColor, size: circleSize))
                                    }
                                }
                        )
                    }
                )
                .preferredColorScheme(.light)
                .navigationBarHidden(true)
                
                VStack {
                    HStack {
                        Spacer()
                        Button(action: {
                            circleSize = 20
                        }) {
                            Image(systemName: "paintbrush.fill")
                                .padding()
                                .background(Color.cyan)
                                .foregroundColor(.white)
                                .cornerRadius(10)
                        }
                        .padding(.trailing, 10)
                        
                        Button(action: {
                            circleSize = 10
                        }) {
                            Image(systemName: "paintbrush.pointed.fill")
                                .padding()
                                .background(Color.cyan)
                                .foregroundColor(.white)
                                .cornerRadius(10)
                        }
                        .padding(.trailing, 20)
                    }
                    Spacer()
                }
            }
        }
    }
    
    func takeScreenshot() -> UIImage? {
        guard let window = UIApplication.shared.windows.first else { return nil }
        UIGraphicsBeginImageContextWithOptions(window.bounds.size, false, UIScreen.main.scale)
        window.drawHierarchy(in: window.bounds, afterScreenUpdates: true)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot
    }
    
    func processImageWithML(image: UIImage) {
        guard let model = try? VNCoreMLModel(for: mlm().model) else {
            print("Failed to load ML model")
            return
        }
        
        let request = VNCoreMLRequest(model: model) { request, error in
            guard let results = request.results as? [VNClassificationObservation],
                  let topResult = results.first
            else {
                print("Failed to make a prediction")
                return
            }
            
            DispatchQueue.main.async {
                self.predictionResult = "Prediction: \(topResult.identifier)"
                if topResult.identifier == "Bears" {
                    self.predictionAccuracy = Int(topResult.confidence * 100)
                    print("True")
                    print(predictionAccuracy)
                    print(topResult.confidence)
                } else if topResult.identifier == "NotBears" {
                    print("False")
                    self.predictionAccuracy = Int(100 - topResult.confidence * 100)
                    print(predictionAccuracy)
                    print(topResult.confidence)
                }
                self.showingResultView = true
            }
        }
        
        guard let ciImage = CIImage(image: image) else {
            print("Failed to convert UIImage to CIImage")
            return
        }
        
        let handler = VNImageRequestHandler(ciImage: ciImage)
        do {
            try handler.perform([request])
        } catch {
            print("Failed to perform classification: \(error.localizedDescription)")
        }
    }
}

struct CircleData: Identifiable {
    var id = UUID()
    var center: CGPoint
    var color: Color
    var size: CGFloat
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
