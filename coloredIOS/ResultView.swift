//
//  endingpage.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 21/05/24.
//

import SwiftUI

struct ResultView: View {
    let accuracy: Int
    let predictionResult : String
    var width = 300.5
    var height = 300.5
    var image: UIImage?
    
    var body: some View {
        NavigationStack{
            VStack {
                Spacer()
                Text("\(accuracy)%")
                    .font(Font.system(size: 80))
                    .padding()
                    .foregroundColor(.cyan)
                Spacer()
                HStack{
                    Spacer()
                    if let uiImage = image {
                        Image(uiImage: uiImage)
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 500.5, maxHeight: 500.5)
                            .padding()
                    }
                    Image("coloredbg")
                        .resizable()
                        .scaledToFit()
                        .frame(width: width, height: height)
                        .padding()
                    Spacer()
                    Spacer()
                }
//                Text("Model Accuracy")
//                    .font(.title)
//                    .padding()
//                Text(predictionResult)
//                    .padding()
                Spacer()
                
                NavigationLink(destination: mainmenu().navigationBarBackButtonHidden()) {
                    Image(systemName: "house.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 50, height: 100)
                        .padding()
                        .foregroundColor(.cyan)
                        .background(Color.white)
                        .clipShape(Circle())
                }
                Spacer()
            }
            .preferredColorScheme(.light)
            .navigationBarBackButtonHidden()
        }
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        ResultView(accuracy: 0, predictionResult: "Prediction: None") // Example accuracy value
    }
}
