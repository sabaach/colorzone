//
//  AudioManager.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 19/05/24.
//

import AVFoundation

class AudioManager {
    static let shared = AudioManager()
    
    private var audioPlayer: AVAudioPlayer?
    
    private init() {}
    
    func playBackgroundMusic() {
        if let audioURL = Bundle.main.url(forResource: "Super Mario Bros. Theme - The Super Mario Bros. Movie Soundtrack (256)", withExtension: "mp3") {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: audioURL)
                audioPlayer?.numberOfLoops = -1
                audioPlayer?.prepareToPlay()
                audioPlayer?.play()
            } catch {
                print("Error playing background music: \(error.localizedDescription)")
            }
        }
    }
    
    func stopBackgroundMusic() {
        audioPlayer?.stop()
    }
}
