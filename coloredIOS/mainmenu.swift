//
//  mainmenu.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 19/05/24.
//

import SwiftUI

struct mainmenu: View {
    var width = 150.5
    var height = 50.5
    
    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                
                Image("ColorZone")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 700, height: 600)
                    .edgesIgnoringSafeArea(.all)
                    .padding(.bottom, 30)
                
                NavigationLink(destination: quest().navigationBarBackButtonHidden(true)) {
                    Text("Colors Game")
                        .padding()
                        .frame(width: width, height: height)
                        .background(Color.cyan)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
                .padding(.bottom, 20)
                
                NavigationLink(destination: Contentvieww().navigationBarBackButtonHidden(true)) {
                    Text("Guess Colors?")
                        .padding()
                        .frame(width: width, height: height)
                        .background(Color.cyan)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
                
                Spacer()
            }
            .background(Color.white.edgesIgnoringSafeArea(.all))
        }
        .preferredColorScheme(.light)
        .onAppear {
            AudioManager.shared.playBackgroundMusic()
        }
        .onDisappear {
            AudioManager.shared.stopBackgroundMusic()
        }
    }
}

#Preview {
    mainmenu()
}
