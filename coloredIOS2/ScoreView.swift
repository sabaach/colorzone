//
//  ScoreView.swift
//  coloredIOS
//
//  Created by Syafrie Bachtiar on 25/05/24.
//

// ScoreView.swift
import SwiftUI

struct ScoreView: View {
    var score: Int

    var body: some View {
        NavigationStack{
            VStack {
                Spacer()
                Text("Game Over")
                    .font(.largeTitle)
                    .foregroundColor(.blue)
                    .bold()
                    .padding()
                Text("Your Score: \(score)")
                    .font(Font.system(size: 80))
                    .foregroundColor(.cyan)
                    .padding()
                Spacer()
                NavigationLink(destination: mainmenu().navigationBarBackButtonHidden()) {
                    Image(systemName: "house.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 50, height: 100)
                        .padding()
                        .foregroundColor(.cyan)
                        .background(Color.white)
                        .clipShape(Circle())
                }
                .padding(.bottom, 50)
            }
        }
    }
}

struct ScoreView_Previews: PreviewProvider {
    static var previews: some View {
        ScoreView(score: 10)
    }
}

