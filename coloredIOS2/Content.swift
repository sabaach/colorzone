//
//  ContentView.swift
//  coloredIOS2
//
//  Created by Syafrie Bachtiar on 17/05/24.
//
// ContentView.swift
import SwiftUI
import AVFoundation
import UIKit
import CoreML
import Vision

struct Content: UIViewControllerRepresentable {
    @Binding var isGameActive: Bool
    @Binding var finalScore: Int
    
    func makeUIViewController(context: Context) -> CameraViewController {
        let viewController = CameraViewController()
        viewController.onGameEnd = {
            self.isGameActive = false
            self.finalScore = viewController.score
        }
        return viewController
    }

    func updateUIViewController(_ uiViewController: CameraViewController, context: Context) {}

    class CameraViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
        var labelIdentifier: UILabel = {
            let label = UILabel()
            label.backgroundColor = .cyan
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = .black
            return label
        }()
        
        var scoreLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = .cyan
            label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
            label.text = "Score: 0"
            return label
        }()
        
        var timeRemainingLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = .cyan
            label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
            label.text = "Time: 15"
            return label
        }()
        
        var imageView: UIImageView = {
            let imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()
        
        var captureSession: AVCaptureSession?
        var model: VNCoreMLModel?
        var timer: Timer?
        var countdownTimer: Timer?
        var score = 0 {
            didSet {
                scoreLabel.text = "Score: \(score)"
            }
        }
        var timeRemaining = 60 {
            didSet {
                timeRemainingLabel.text = "Time: \(timeRemaining)"
            }
        }
        var currentImageName: String?
        var onGameEnd: (() -> Void)?

        override func viewDidLoad() {
            super.viewDidLoad()
            setupCamera()
            setupIdentifierConfidenceLabel()
            setupScoreLabel()
            setupTimeRemainingLabel()
            setupImageView()
            setupMLModel()
            startTimer()
            startCountdown()
        }

        private func setupCamera() {
                    captureSession = AVCaptureSession()
                    captureSession?.sessionPreset = .photo

                    guard let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else { return }
                    guard let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice) else { return }

                    if let captureSession = captureSession,
                       captureSession.canAddInput(videoDeviceInput) {
                        captureSession.addInput(videoDeviceInput)
                    }

                    let videoOutput = AVCaptureVideoDataOutput()
                    videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))

                    if let captureSession = captureSession,
                       captureSession.canAddOutput(videoOutput) {
                        captureSession.addOutput(videoOutput)
                    }

                    let videoLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                    videoLayer.frame = view.bounds
                    videoLayer.videoGravity = .resizeAspectFill

                    view.layer.addSublayer(videoLayer)
                    captureSession?.startRunning()
                }

        private func setupIdentifierConfidenceLabel() {
            view.addSubview(labelIdentifier)
            labelIdentifier.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32).isActive = true
            labelIdentifier.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            labelIdentifier.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            labelIdentifier.heightAnchor.constraint(equalToConstant: 50).isActive = true
        }

        private func setupScoreLabel() {
            view.addSubview(scoreLabel)
            scoreLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
            scoreLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        }

        private func setupTimeRemainingLabel() {
            view.addSubview(timeRemainingLabel)
            timeRemainingLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
            timeRemainingLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        }

        private func setupImageView() {
            view.addSubview(imageView)
            imageView.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor, constant: 20).isActive = true
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        }

        private func setupMLModel() {
            do {
                model = try VNCoreMLModel(for: mlcolor().model)
            } catch {
                print("Failed to load ML model:", error)
            }
        }

        private func startTimer() {
            timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(updateImage), userInfo: nil, repeats: true)
            updateImage()
        }

        private func startCountdown() {
            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeRemaining), userInfo: nil, repeats: true)
        }

        @objc private func updateImage() {
            let randomImageIndex = Int.random(in: 1...3)
            let imageName = "image\(randomImageIndex)"
            currentImageName = imageName
            imageView.image = UIImage(named: imageName)
        }

        @objc private func updateTimeRemaining() {
            timeRemaining -= 1
            if timeRemaining <= 0 {
                endGame()
            }
        }

        private func endGame() {
            timer?.invalidate()
            countdownTimer?.invalidate()
            onGameEnd?()
        }

        func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
            guard let model = model else { return }
            guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

            let request = VNCoreMLRequest(model: model) { [weak self] (finishRequest, error) in
                guard let results = finishRequest.results as? [VNClassificationObservation] else { return }
                guard let observation = results.first else { return }

                DispatchQueue.main.async {
                    self?.labelIdentifier.text = "\(observation.identifier)"
                    self?.updateScore(with: observation.identifier)
                }
            }

            try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
        }

        private func updateScore(with identifier: String) {
            if (identifier == "Merah" && currentImageName == "image3") ||
               (identifier == "Hijau" && currentImageName == "image2") ||
               (identifier == "Biru" && currentImageName == "image1") {
                score += 1
            }
        }
    }
}

struct Contentvieww: View {
    @State private var isGameActive = true
    @State private var finalScore = 0

    var body: some View {
        NavigationStack{
            Group {
                if isGameActive {
                    Content(isGameActive: $isGameActive, finalScore: $finalScore)
                        .edgesIgnoringSafeArea(.all)
                } else {
                    ScoreView(score: finalScore)
                }
            }
        }
    }
}

struct Contentvieww_Previews: PreviewProvider {
    static var previews: some View {
        Contentvieww()
    }
}
